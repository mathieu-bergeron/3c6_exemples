package demo1_2;


public class Triangle extends Forme{

    private int angle;

    public Triangle(double largeur, double hauteur, int angle)  {
        super(largeur, hauteur);

        this.angle = angle;
    }

    @Override
    protected String nom() {
        return "Triangle";
    }

}
