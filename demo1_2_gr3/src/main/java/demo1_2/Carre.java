package demo1_2;

public class Carre extends Forme {

    public Carre(double taille) {
        super(taille, taille);
    }



    @Override
    public double largeurEnMm(){

        //boolean siLargeurEgalHauteur = true;

        double largeur = super.largeurEnMm();
        double hauteur = super.hauteurEnMm();

        if(largeur != hauteur){
            throw new RuntimeException("Aaaaaaah! Hauteur pas comme largeur pour un carré");
        }


        return largeur;

    }



    @Override
    protected String nom() {
        return "Carré";
    }

}
