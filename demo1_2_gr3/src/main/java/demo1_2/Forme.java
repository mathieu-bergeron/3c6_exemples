package demo1_2;

import java.io.PrintStream;

public abstract class Forme implements Affichable{

    public Forme(double largeur, double hauteur){

        this.largeurEnMetres = largeur / 1000;
        this.hauteurEnMetres = hauteur / 1000;


    }

//    private String typeDeForme = "";


    private double largeurEnMetres; // la largeur est en metres
    private double hauteurEnMetres;

    public double largeurEnMm(){
        return largeurEnMetres * 1000;

    }

    public double hauteurEnMm(){
        return hauteurEnMetres * 1000;

    }

    public void afficher(PrintStream out) {
        String nom = this.nom();


        out.println(nom + ": " + largeurEnMetres + ", " + hauteurEnMetres);
    }

    protected abstract String nom();

}
