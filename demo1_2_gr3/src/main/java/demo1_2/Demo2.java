package demo1_2;

import demo1_2.validateur.Modele;

@SuppressWarnings("serial")

public class Demo2 extends Modele<Demo2> {
	
	public static void main(String[] args) {
		new Demo2().initialize();
	}
	
	
	
	private Forme[] formes = new Forme[3];
	private Affichable[] affichable = new Affichable[3];
	private Usager usager = new Usager();
	
	@Override
	public void initialize() {

		formes[0] = null;
		formes[1] = new Carre(12.0);
		formes[2] = new Triangle(34.5, 22, 45);


		//formes[0].afficher(System.out);
		formes[1].afficher(System.err);
		formes[2].afficher(System.out);



		usager.afficher(System.out);

		affichable[0] = new Usager();
		affichable[1] = new Carre(12.0);
		
		
		
	}

}
