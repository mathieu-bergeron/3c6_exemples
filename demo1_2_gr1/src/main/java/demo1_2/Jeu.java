package demo1_2;

public abstract class Jeu {
	
	private String titre;
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	
	public Jeu(String titre) {
		setTitre(titre);
	}

	public void ouvrirMenu() {
		
		
	}
	
	public abstract String categorie();

}
