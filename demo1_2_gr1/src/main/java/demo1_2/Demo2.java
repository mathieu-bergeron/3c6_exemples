package demo1_2;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.internal.operation.SyncOperations;

import demo1_2.validateur.Modele;

@SuppressWarnings("serial")

public class Demo2 extends Modele<Demo2> {
	
	
	private Jeu[] lesJeux = new Jeu[4];
	
	@Override
	public void initialize() {
		
		lesJeux[0] = new JeuSociete("Risk");
		lesJeux[1] = new JeuStrategie("Stratego");
		lesJeux[2] = new JeuVideo("FIFA");
		lesJeux[3] = new JeuVideoAction("Contra");
		
		for(int i = 0; i < lesJeux.length; i++) {
			Jeu jeuCourant = lesJeux[i];
			String categorie = "";
			
			if(jeuCourant instanceof JeuStrategie) {

				categorie = "JeuStrategie";
				
			}else if(jeuCourant instanceof JeuSociete) {
				
				categorie = "JeuSociete";
				
			}else if(jeuCourant instanceof JeuVideo) {

				categorie = "JeuVideo";
				
			}
			
			System.out.println(categorie + ": " + jeuCourant.getTitre());
		}

		for(int i = 0; i < lesJeux.length; i++) {
			Jeu jeuCourant = lesJeux[i];
			System.out.println(jeuCourant.getClass().getSimpleName() + ": " + jeuCourant.getTitre());
		}

		for(int i = 0; i < lesJeux.length; i++) {
			Jeu jeuCourant = lesJeux[i];
			System.out.println(jeuCourant.categorie() + ": " + jeuCourant.getTitre());
		}
	
	}
	
	public static void main(String[] args) {
		new Demo2().initialize();
	}

}
